package model;

import model.impl.drinks.BlackTea;
import model.impl.toppings.Milk;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OrderTest {
    @Test
    public void teaWithMilk() {
        Order order = new Order();

        Drink tea = new BlackTea(200);
        Topping milk = new Milk(20);
        tea.add(milk);

        order.add(tea);

        assertEquals("Black tea with Milk \t15 X 1", OrderPrinter.print(order));
    }
}
