package model;

import java.math.BigDecimal;


@SuppressWarnings("unused")
public interface Priced {
    BigDecimal price();
}
