package model;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Order {
    private final Map<Drink, Integer> drinks = new HashMap<>();

    public void add(Drink drink) {
        drinks.compute(drink, (k, v) -> v == null ? 1 : v + 1);
    }

    public Map<Drink, Integer> getDrinks() {
        return drinks;
    }
}
