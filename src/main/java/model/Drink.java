package model;

import java.math.BigDecimal;
import java.util.*;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class Drink implements Priced, Descriptive {
    private final List<Topping> toppings = new ArrayList<>();
    private final int volume;
    private final String name;
    private final BigDecimal price;

    public void add(Topping topping) {
        toppings.add(topping);
    }

    public boolean hasToppings() {
        return !toppings.isEmpty();
    }

    public List<Topping> getToppings() {
        return Collections.unmodifiableList(toppings);
    }

    @Override
    public String description() {
        return name;
    }

    @Override
    public String additionalInfo() {
        return name + " " + volume + " ml";
    }

    @Override
    public BigDecimal price() {
        BigDecimal priceWithToppings = new BigDecimal(price.doubleValue());
        for (Topping topping : toppings) {
            priceWithToppings = priceWithToppings.add(topping.price());
        }
        return priceWithToppings;
    }

    protected Drink(int volume, String name, BigDecimal price) {
        this.volume = volume;
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drink drink = (Drink) o;
        return volume == drink.volume &&
                Objects.equals(toppings, drink.toppings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(toppings, volume);
    }
}
