package model.impl.drinks;

import model.Drink;

import java.math.BigDecimal;

@SuppressWarnings("WeakerAccess")
public abstract class Tea extends Drink {

    protected Tea(int volume, String name, BigDecimal price) {
        super(volume, name, price);
    }
}
