package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class GreenTea extends Tea {

    public GreenTea(int volume) {
        super(volume, "Green tea", BigDecimal.valueOf(18));
    }
}
