package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class WhiteTea extends Tea {

    public WhiteTea(int volume) {
        super(volume, "White tea", BigDecimal.valueOf(17));
    }
}
