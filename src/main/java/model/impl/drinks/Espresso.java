package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Espresso extends Coffee {

    public Espresso(int volume) {
        super(volume, "Espresso", BigDecimal.valueOf(19));
    }
}
