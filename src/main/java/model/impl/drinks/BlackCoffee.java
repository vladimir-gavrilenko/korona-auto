package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class BlackCoffee extends Coffee {

    public BlackCoffee(int volume) {
        super(volume, "Black coffee", BigDecimal.valueOf(18));
    }
}
