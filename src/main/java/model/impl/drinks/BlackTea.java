package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class BlackTea extends Tea {

    public BlackTea(int volume) {
        super(volume, "Black tea", BigDecimal.valueOf(10));
    }
}
