package model.impl.drinks;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Latte extends Coffee {

    public Latte(int volume) {
        super(volume, "Latte", BigDecimal.valueOf(20));
    }
}
