package model.impl.drinks;

import model.Drink;

import java.math.BigDecimal;

@SuppressWarnings("WeakerAccess")
public abstract class Coffee extends Drink {
    protected Coffee(int volume, String name, BigDecimal price) {
        super(volume, name, price);
    }
}
