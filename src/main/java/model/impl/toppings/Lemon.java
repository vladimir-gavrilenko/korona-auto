package model.impl.toppings;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Lemon extends OtherTopping {
    public Lemon() {
        super("Lemon", "-", BigDecimal.valueOf(3));
    }
}
