package model.impl.toppings;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Honey extends SweetTopping {
    public Honey(double amount) {
        super("Honey", amount + "%", BigDecimal.valueOf(6 * amount));
    }
}
