package model.impl.toppings;

import model.Topping;

import java.math.BigDecimal;

@SuppressWarnings("WeakerAccess")
public abstract class OtherTopping extends Topping {
    protected OtherTopping(String name, String additionalInfo, BigDecimal price) {
        super(name, additionalInfo, price);
    }
}
