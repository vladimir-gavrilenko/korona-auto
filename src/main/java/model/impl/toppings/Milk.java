package model.impl.toppings;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Milk extends MilkyTopping {
    public Milk(double fatness) {
        super("Milk", String.valueOf(fatness) + "%", BigDecimal.valueOf(5));
    }
}
