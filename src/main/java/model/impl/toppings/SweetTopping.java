package model.impl.toppings;

import model.Topping;

import java.math.BigDecimal;

@SuppressWarnings("WeakerAccess")
public abstract class SweetTopping extends Topping {
    protected SweetTopping(String name, String additionalInfo, BigDecimal price) {
        super(name, additionalInfo, price);
    }
}
