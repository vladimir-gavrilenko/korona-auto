package model.impl.toppings;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Cream extends MilkyTopping {
    public Cream(double fatness) {
        super("Cream", String.valueOf(fatness) + "%", BigDecimal.valueOf(7));
    }
}
