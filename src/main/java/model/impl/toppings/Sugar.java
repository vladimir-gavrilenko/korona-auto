package model.impl.toppings;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Sugar extends SweetTopping {
    public Sugar(double amount) {
        super("Sugar", amount + "%", BigDecimal.valueOf(3 * amount));
    }
}
