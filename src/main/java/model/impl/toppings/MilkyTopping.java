package model.impl.toppings;

import model.Topping;

import java.math.BigDecimal;

@SuppressWarnings("WeakerAccess")
public abstract class MilkyTopping extends Topping {
    protected MilkyTopping(String name, String additionalInfo, BigDecimal price) {
        super(name, additionalInfo, price);
    }
}
