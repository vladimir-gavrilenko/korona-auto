package model;

import java.math.BigDecimal;

// TODO write normal implementation
@SuppressWarnings("all")
public class OrderPrinter {
    public static String print(Order order) {
        StringBuilder resultBuilder = new StringBuilder();
        BigDecimal total = BigDecimal.valueOf(0);
        order.getDrinks().forEach((Drink drink, Integer count) -> {
            resultBuilder.append(drink.description());
            if (drink.hasToppings()) {
                resultBuilder.append(" with ");
                for (Topping topping : drink.getToppings()) { // ignore commas for short
                    resultBuilder.append(topping.description()).append(" ");
                }
            }
            resultBuilder.append("\t")
                .append(drink.price().toString())
                .append(" X ").append(count)
                .append("\n");

        });
        return resultBuilder.toString();
    }
}
