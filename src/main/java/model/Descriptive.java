package model;

@SuppressWarnings("unused")
public interface Descriptive {

    String description();

    String additionalInfo();

}
