package model;

import java.math.BigDecimal;

public abstract class Topping implements Priced, Descriptive {
    private final String name;
    private final String additionalInfo;
    private final BigDecimal price;

    protected Topping(String name, String additionalInfo, BigDecimal price) {
        this.name = name;
        this.additionalInfo = additionalInfo;
        this.price = price;
    }

    protected String getName() {
        return name;
    }

    @Override
    public String description() {
        return name;
    }

    @Override
    public String additionalInfo() {
        return name + " " + additionalInfo;
    }

    @Override
    public BigDecimal price() {
        return price;
    }
}
